#!/usr/bin/python3

import re
import sys


def make_pep440_compliant(version: str) -> str:
    """Convert the version into a PEP440 compliant version."""
    public_version_re = re.compile(
        r"^([0-9][0-9.]*(?:(?:a|b|rc|.post|.dev)[0-9]+)*)\+?"
    )
    _, public, local = public_version_re.split(version, maxsplit=1)
    if not local:
        return version
    sanitized_local = re.sub("[+~]+", ".", local).strip(".")
    pep440_version = f"{public}+{sanitized_local}"
    assert re.match(
        "^[a-zA-Z0-9.]+$", sanitized_local
    ), f"'{pep440_version}' not PEP440 compliant"
    return pep440_version


if __name__ == "__main__":
    print(make_pep440_compliant(sys.argv[1]))
